# ft_irc

## About
IRC-compliant server that serves simultaneously hundreds to thousands of users. Coded in pre-modern C++98, this project is among the advanced ones in the Common Core of Ecole 42's curriculum.

## How-to
Run the `make` command to compile the project and produce an executable named `ircserv`. 
Run this executable in the terminal with arguments `./ircserv [port] [password]` to start the IRC server with a specified port and server's password. It is necessary to specify `""` (empty double-quoted string) for an empty password.
Connect to the server using your favorite client and start chatting!

## Bonus
A file `bot.py` was added as part of the project's bonus points, which was to have a bot automatically added to the server. This bot does nothing than calculating simple math, and retrieving some random quotes from the Internet. Please don't judge!

For debugging, I also coded a script `proxy.py` which bridges connection between IRC client and server during development. This Python script intercepts communications from both ways to print to `stdout` the exact messages, character-by-character, that are sent between a referent client (HexChat) and a referent server (inspIRCd). This simple but powerful trick relieved me from having to install and configure more robust but overkill tool like Wireshark.
